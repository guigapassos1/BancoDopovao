package br.usal.bes20181.bancoDoPovao.BO;

import java.util.Scanner;

import br.usal.bes20181.bancoDoPovao.DOMAIN.ContaDOMAIN;
import br.usal.bes20181.bancoDoPovao.TUI.ContaTUI;
import br.usal.bes20181.bancoDoPovao.TUI.CorrentistaTUI;

public class ContaBO {

	Scanner sc = new Scanner(System.in);

	public void opcaoDaConta(ContaDOMAIN contaDOMAIN) {
		contaDOMAIN.setEscolha(sc.nextInt());
		if (contaDOMAIN.getEscolha() == 1) {
			depositar(contaDOMAIN);
		} else if (contaDOMAIN.getEscolha() == 2) {
			sacar(contaDOMAIN);
		} else if (contaDOMAIN.getEscolha() == 3) {
			creditar(contaDOMAIN);
		} else if (contaDOMAIN.getEscolha() == 4) {
			mudarSituacao(contaDOMAIN);
		}
		CorrentistaTUI.contaCriada();
		ContaTUI.entrarNaConta();
		opcaoDaConta(contaDOMAIN);
	}

	private void mudarSituacao(ContaDOMAIN contaDOMAIN) {
		System.out.println("Digite a situa��o atual da conta:\nManuten��o (M)\nAtiva (A)\nEncerrada (E)");
		contaDOMAIN.setSituacaoConta(sc.next().toUpperCase());
		if(ContaDOMAIN.getSituacaoConta().equals("M")) {
			contaDOMAIN.setSituacaoConta("Manuten��o");
		}else if(ContaDOMAIN.getSituacaoConta().equals("A")) {
			contaDOMAIN.setSituacaoConta("Ativa");
		} else if (ContaDOMAIN.getSituacaoConta().equals("E")) {
			contaDOMAIN.setSituacaoConta("Encerrada");
		}
	}

	private void creditar(ContaDOMAIN contaDOMAIN) {
		System.out.println("Digite quanto deseja ter de cr�dito:");
		contaDOMAIN.setCredito(sc.nextDouble());
	}

	private void sacar(ContaDOMAIN contaDOMAIN) {
		System.out.println("Digite quanto deseja sacar:");
		contaDOMAIN.setSaque(sc.nextDouble());
		contaDOMAIN.setSaldo(ContaDOMAIN.getSaldo() - ContaDOMAIN.getSaque());
	}

	private void depositar(ContaDOMAIN contaDOMAIN) {
		System.out.println("Digite quanto deseja depositar:");
		contaDOMAIN.setDeposito(sc.nextDouble());
		contaDOMAIN.setSaldo(ContaDOMAIN.getSaldo() + ContaDOMAIN.getDeposito());
	}
}
