package br.usal.bes20181.bancoDoPovao.BO;

import java.util.Scanner;

import br.usal.bes20181.bancoDoPovao.DOMAIN.ContaDOMAIN;
import br.usal.bes20181.bancoDoPovao.DOMAIN.CorrentistaDOMAIN;
import br.usal.bes20181.bancoDoPovao.TUI.ContaTUI;
import br.usal.bes20181.bancoDoPovao.TUI.CorrentistaTUI;

public class CorrentistaBO {

	Scanner sc = new Scanner(System.in);

	public void cadastrarOuAcessessar(CorrentistaDOMAIN correntistaDOMAIN, ContaDOMAIN contaDOMAIN) {
		CorrentistaTUI.entrarNoBanco();
		correntistaDOMAIN.setEscolha(sc.nextInt());
		if (correntistaDOMAIN.getEscolha() == 1) {
			criarConta(correntistaDOMAIN, contaDOMAIN);
			cadastrarOuAcessessar(correntistaDOMAIN, contaDOMAIN);
		} else if (correntistaDOMAIN.getEscolha() == 2) {
			acessarConta(correntistaDOMAIN, contaDOMAIN);
		}
	}

	private void acessarConta(CorrentistaDOMAIN correntistaDOMAIN, ContaDOMAIN contaDOMAIN) {
		if (contaDOMAIN.getNumeroConta() == 0) {
			System.out.println("Nenhuma conta registrada no momento");
			cadastrarOuAcessessar(correntistaDOMAIN, contaDOMAIN);
		} else if (contaDOMAIN.getNumeroConta() >= 1) {
			ContaTUI.econtaTUI();
		}
	}

	private void criarConta(CorrentistaDOMAIN correntistaDOMAIN, ContaDOMAIN contaDOMAIN) {
		CorrentistaTUI.criarConta();
		verificarDocumentos(correntistaDOMAIN);
		contaDOMAIN.setNumeroConta(1 + contaDOMAIN.getNumeroConta());
	}

	private void verificarDocumentos(CorrentistaDOMAIN correntistaDOMAIN) {
		if (correntistaDOMAIN.getDocumentos().toUpperCase().equals("S")) {
			correntistaDOMAIN.setDocumentos("OK");
			CorrentistaTUI.contaCriada();
		} else if (correntistaDOMAIN.getDocumentos().toUpperCase().equals("N")) {
			System.out.println(
					"Falha na cria��o da conta\nPara cria��o da conta, � necessario ter os devidos documentos");
		}
	}
}