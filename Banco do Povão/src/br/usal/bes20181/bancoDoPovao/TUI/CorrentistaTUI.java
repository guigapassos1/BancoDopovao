package br.usal.bes20181.bancoDoPovao.TUI;

import java.util.Scanner;

import br.usal.bes20181.bancoDoPovao.BO.CorrentistaBO;
import br.usal.bes20181.bancoDoPovao.DOMAIN.ContaDOMAIN;
import br.usal.bes20181.bancoDoPovao.DOMAIN.CorrentistaDOMAIN;

public class CorrentistaTUI {

	static Scanner sc = new Scanner(System.in);
	CorrentistaBO correntistaBO = new CorrentistaBO();
	static CorrentistaDOMAIN correntistaDOMAIN= new CorrentistaDOMAIN();
	static ContaDOMAIN contaDOMAIN= new ContaDOMAIN();

	public void eCorrentistaTUI() {
		correntistaBO.cadastrarOuAcessessar(correntistaDOMAIN, contaDOMAIN);
	}

	public static void criarConta() {
		System.out.println("-----Cria��o de Conta-----");
		System.out.println("Nome do cliente:");
		correntistaDOMAIN.setNome(sc.next());
		System.out.println("CPF do cliente:");
		correntistaDOMAIN.setCpf(sc.next());
		System.out.println("Endere�o do cliente:");
		correntistaDOMAIN.setEndereco(sc.next());
		System.out.println("Telefone do cliente:");
		correntistaDOMAIN.setTelefone(sc.next());
		System.out.println("RG, comprovante de resid�ncia e comprovante de renda entregues? (S/N)");
		correntistaDOMAIN.setDocumentos(sc.next());
		contaDOMAIN.setSituacaoConta("ATIVA");
		System.out.println("-----Cria��o de Conta-----\n");
		System.out.println("Conta criada com sucesso!!!\n");
	}

	public static void contaCriada() {
		System.out.println("-----Conta-----");
		System.out.println("Nome: " + correntistaDOMAIN.getNome());
		System.out.println("CPF: " + correntistaDOMAIN.getCpf());
		System.out.println("Endere�o: " + correntistaDOMAIN.getEndereco());
		System.out.println("Telefone: " + correntistaDOMAIN.getTelefone());
		System.out.println("Documentos: " + correntistaDOMAIN.getDocumentos());
		System.out.println("Saldo: "+ ContaDOMAIN.getSaldo());
		System.out.println("Cr�dito: "+ ContaDOMAIN.getCredito());
		System.out.println("Situa��o: "+ ContaDOMAIN.getSituacaoConta());
		System.out.println("-----Conta-----\n");
	}

	public static void entrarNoBanco() {
		System.out.println("Bem vindo ao Banco do Pov�o\n");
		System.out.println("Para cadastrar conta: 1");
		System.out.println("Para acessar conta: 2");
	}

}