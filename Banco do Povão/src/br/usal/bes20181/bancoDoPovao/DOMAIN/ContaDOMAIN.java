package br.usal.bes20181.bancoDoPovao.DOMAIN;

public class ContaDOMAIN {

	private int numeroConta;
	private static double saldo = 0;
	private static double credito = 0;
	private static double saque;
	private static double deposito;
	private int consultaConta;
	private static String situacaoConta;
	private int escolha;

	public int getEscolha() {
		return escolha;
	}

	public void setEscolha(int escolha) {
		this.escolha = escolha;
	}

	public int getNumeroConta() {
		return numeroConta;
	}

	public void setNumeroConta(int numeroConta) {
		this.numeroConta = numeroConta;
	}

	public static double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		ContaDOMAIN.saldo = saldo;
	}

	public static double getCredito() {
		return credito;
	}

	public void setCredito(double credito) {
		ContaDOMAIN.credito = credito;
	}

	public static double getSaque() {
		return saque;
	}

	public void setSaque(double saque) {
		ContaDOMAIN.saque = saque;
	}

	public static double getDeposito() {
		return deposito;
	}

	public void setDeposito(double deposito) {
		ContaDOMAIN.deposito = deposito;
	}

	public int getConsultaConta() {
		return consultaConta;
	}

	public void setConsultaConta(int consultaConta) {
		this.consultaConta = consultaConta;
	}

	public static String getSituacaoConta() {
		return situacaoConta;
	}

	public void setSituacaoConta(String situacaoConta) {
		ContaDOMAIN.situacaoConta = situacaoConta;
	}

}
